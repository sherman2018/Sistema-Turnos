export class Especialidad {
    id: number;
    nombreEspecialidad: string;

    constructor (obj?: any) {
        this.id = obj && obj.id || this.randomIntFromInterval(-100000, 0);
        this.nombreEspecialidad = obj && obj.nombreEspecialidad || '';
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }
}
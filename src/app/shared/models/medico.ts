import { Especialidad } from '../../shared/models/especialidad'; 

export class Medico {
    id: number;
    apellido: string;
    nombre: string;
    dni: number;
    cuit: string;
    fechaNacimiento: any;
    especialidades: Especialidad[];
    obraSocial: string;

    constructor(obj?: any) {
        this.id = obj && obj.id || this.randomIntFromInterval(-100000, 0);
        this.apellido = obj && obj.apellido || '';
        this.nombre = obj && obj.nombre || '';
        this.dni = obj && obj.dni || '';
        this.cuit = obj && obj.cuit || '';
        this.fechaNacimiento = obj && obj.fechaNacimiento || '';
        this.especialidades = obj && obj.especialidades || '';
        this.obraSocial = obj && obj.obraSocial || '';
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
}
import { NgModule } from '@angular/core';
import { TurnosComponent } from '../../turnos/turnos.component';
import { MedicosComponent } from '../../medicos/medicos.component';
import { ShellComponent } from '../shell/shell.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../../home/home.component';

const rutas: Routes = [
  {path: '', component: HomeComponent},
  {path: 'turnos', component: TurnosComponent},
  {path: 'medicos', component: MedicosComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(rutas)
  ],
  exports: [ RouterModule ] 
})
export class CoreRoutingModule { }

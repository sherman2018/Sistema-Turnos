import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'turnos-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title='Sistema de Turnos';
  constructor() { }

  ngOnInit() {
  }

}

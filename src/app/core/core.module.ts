import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from './shell/shell.component';
import { HeaderComponent } from './shell/header/header.component';
import { LeftMenuComponent } from './shell/left-menu/left-menu.component';
import { FooterComponent } from './shell/footer/footer.component';
import { ContentComponent } from './shell/content/content.component';
import { TurnosComponent } from '../turnos/turnos.component';
import { MedicosComponent } from '../medicos/medicos.component';
import { HomeComponent } from '../home/home.component';
import { CoreRoutingModule } from './core-routing/core-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  declarations: [
    ShellComponent,
    TurnosComponent,
    MedicosComponent,
    HomeComponent, 
    HeaderComponent, 
    LeftMenuComponent, 
    FooterComponent, ContentComponent
  ],
    exports: [
      ShellComponent
    ]
})
export class CoreModule { }
